<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('font-awesome-4.7.0/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body style="background: #f6f6f6">
    <div class="tp-refund">
        <div class="header">
            <span>Trả hàng/ Hoàn tiền</span>
        </div>
        <div class="container" style="margin-top: 44px">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="content">
                        <div class="txt-select">
                            <span style="font-size: 13px; color: #222">Vui lòng chọn sản phẩm để Trả hàng/Hoàn tiền</span>
                        </div>
                        <div class="store-info">
                            <input type="checkbox" value="">
                            <img style="width: 24px; height: 24px; border-radius: 50%" src="https://cf.shopee.vn/file/8bec6e6bc6b085760e6fdf0247830a87_tn">
                            <span>todex.vn</span>
                        </div>
                        <div class="txt-delivered">
                            <span>Kiện hàng 1</span>
                            <span style="color: #00BFA5; font-size: 12px">Đã giao</span>
                        </div>
                        <div class="item d-flex">
                            <input type="checkbox" value="">
                            <div style="border: 1px solid rgba(0,0,0,0.09); margin-left: 12px"><img style="width: 66px; height: 66px" src="https://cf.shopee.vn/file/72447715b0f8d2b2fcce2bd099ae7158_tn"></div>
                            <div class="item-info" style="max-width: 78%">
                                <div class="name">[Mã ELFLASH11 hoàn xu 10K đơn 20K] TODEX TPU Silicon Strap Replaces Protection For The Xiaomi Mi Band 4 Wristwatch</div>
                                <span>Phân loại:Black</span><span class="float-right" style="color: #222">x 1</span>
                                <div class="price-info">
                                    <span style="text-decoration: line-through">₫205.000</span>
                                    <span style="color: #222; margin-left: 5px">₫1.000</span>
                                </div>
                            </div>
                        </div>
                        <div class="reason">
                            <span style="vertical-align: sub">Lý do</span>
                            <span style="color: #EE4D2D;" class="float-right">Chọn lý do<i class="fa fa-angle-right" aria-hidden="true"></i></span>
                        </div>
                        <div class="upload-image">
                            <img src="{{asset('images/img-ip-upload.png')}}">
                        </div>
                        <div class="note">
                            <p>Chú thích</p>
                            <textarea placeholder="Nhập lý do và thêm hình ảnh chỉ rõ (1) lỗi sản phẩm (2) gói hàng thể hiện rõ mã vận đơn và trạng thái đóng gói"></textarea>
                        </div>
                        <div class="email">
                            <p>Email của bạn</p>
                            <input type="email" placeholder="binhdv62@wru.vn">
                        </div>
                        <button class="btn-submit-refund">
                            Hoàn thành
                        </button>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>
</body>
</html>

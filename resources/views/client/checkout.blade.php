@extends('layouts.app')

@section('content')
    <div class="tp-checkout">
        <div class="container">
            <div class="row">
                <div class="border-delivery"></div>
                <div class="header w-100">
                    <div class="title">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <span>Địa Chỉ Nhận Hàng</span>
                    </div>
                    <div class="customer-info">
                        <span class="name">pham van quan</span>
                        <span class="phone-number">(+84) 857489827</span>
                        <span class="address">22d 1234, Thị Trấn Nam Giang, Huyện Nam Trực, Nam Định</span>
                        <span class="default-address">Mặc Định</span>
                        <span class="change-address">THAY ĐỔI</span>
                    </div>
                </div>
                <div class="wrap-info-checkout w-100">
                    <div class="group-item-store w-100">
                        <div class="store">
                            <div class="wrap-title d-flex">
                                <div class="col-md-7">
                                    <span style="color: #222; font-size: 1.125rem">Sản phẩm</span>
                                </div>
                                <div class="col-md-5">
                                    <span class="d-inline-block w-25">Đơn giá</span>
                                    <span class="d-inline-block w-25">Số lượng</span>
                                    <span class="d-inline-block float-right">Thành tiền</span>
                                </div>
                            </div>
                            <div class="store-name d-flex">
                                <div class="name">
                                    <i class="fa fa-home" aria-hidden="true"></i>
                                    <span>Quần Tây Nam TCE</span>
                                </div>
                                <div class="chat-now">
                                    <i class="fa fa-comment" aria-hidden="true"></i>
                                    <span>Chat ngay</span>
                                </div>
                            </div>
                            <div class="list-item">
                                <div class="item d-flex">
                                    <div class="col-md-7">
                                        <img style="width: 40px; height: 40px" src="https://cf.shopee.vn/file/fc9c996c33e5e0a6c8920fb00526db0d_tn">
                                        <span class="name d-inline-block">Quần Âu Nam Ikemen Côn Ống Dáng Ôm Vả dndd dmdmd mdmdmd</span>
                                        <span class="type d-inline-block">Loại: - GHI SÁNG -,SIZE 29</span>
                                    </div>
                                    <div class="col-md-5">
                                        <span class="d-inline-block w-25">₫72.000</span>
                                        <span class="d-inline-block w-25">1</span>
                                        <span class="float-right">₫72.000</span>
                                    </div>
                                </div>
                                <div class="item d-flex">
                                    <div class="col-md-7">
                                        <img style="width: 40px; height: 40px" src="https://cf.shopee.vn/file/fc9c996c33e5e0a6c8920fb00526db0d_tn">
                                        <span class="name d-inline-block">Quần Âu Nam Ikemen Côn Ống Dáng Ôm Vả dndd dmdmd mdmdmd</span>
                                        <span class="type d-inline-block">Loại: - GHI SÁNG -,SIZE 29</span>
                                    </div>
                                    <div class="col-md-5">
                                        <span class="d-inline-block w-25">₫72.000</span>
                                        <span class="d-inline-block w-25">1</span>
                                        <span class="float-right">₫72.000</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="voucher w-100 d-flex">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <i aria-hidden="true" class="fa fa-bookmark-o" style="color: rgb(238, 77, 45); font-size: 20px;"></i>
                            <span>Voucher của Shop</span>
                            <span style="float: right; color: #05a">Chọn Voucher</span>
                        </div>
                    </div>
                    <div class="transport-info w-100 d-flex">
                        <div class="col-md-4 note">
                            <span>Lời nhắn:</span>
                            <input type="text" placeholder="Lưu ý cho Người bán...">
                        </div>
                        <div class="col-md-8 info">
                            <span class="d-inline-block" style="width: 20%; color: #00bfa5">Đơn vị vận chuyển:</span>
                            <div style="width: 50%; padding-left: 20px">
                                <p style="margin-bottom: .3125rem; font-size: 1rem; font-weight: 600">Vận Chuyển Nhanh</p>
                                <p style="font-size: 0.75rem; font-weight: 400; margin: 0">Giao Hàng Tiết Kiệm</p>
                                <p style="font-weight: 400; margin-top: .3125rem;color: #ff424f">Vui lòng chọn thời gian giao hàng mong muốn</p>
                            </div>
                            <span class="d-inline-block text-center" style="width: 15%; color: #05a; text-transform: uppercase; cursor: pointer">Thay đổi</span>
                            <span class="d-inline-block text-center" style="width: 15%">₫37.700</span>
                        </div>
                    </div>
                    <div class="total-price w-100">
                        <div class="">
                            <span style="font-size: .875rem; color: #929292">Tổng số tiền (1 sản phẩm):</span>
                            <span style="font-size: 1.25rem; margin-left: 1.25rem; color: #ee4d2d">₫123.000</span>
                        </div>
                    </div>
                </div>
                <div class="wrap-info-checkout w-100">
                    <div class="group-item-store w-100">
                        <div class="store">
                            <div class="wrap-title d-flex">
                                <div class="col-md-7">
                                    <span style="color: #222; font-size: 1.125rem">Sản phẩm</span>
                                </div>
                                <div class="col-md-5">
                                    <span class="d-inline-block w-25">Đơn giá</span>
                                    <span class="d-inline-block w-25">Số lượng</span>
                                    <span class="d-inline-block float-right">Thành tiền</span>
                                </div>
                            </div>
                            <div class="store-name d-flex">
                                <div class="name">
                                    <i class="fa fa-home" aria-hidden="true"></i>
                                    <span>Quần Tây Nam TCE</span>
                                </div>
                                <div class="chat-now">
                                    <i class="fa fa-comment" aria-hidden="true"></i>
                                    <span>Chat ngay</span>
                                </div>
                            </div>
                            <div class="list-item">
                                <div class="item d-flex">
                                    <div class="col-md-7">
                                        <img style="width: 40px; height: 40px" src="https://cf.shopee.vn/file/fc9c996c33e5e0a6c8920fb00526db0d_tn">
                                        <span class="name d-inline-block">Quần Âu Nam Ikemen Côn Ống Dáng Ôm Vả dndd dmdmd mdmdmd</span>
                                        <span class="type d-inline-block">Loại: - GHI SÁNG -,SIZE 29</span>
                                    </div>
                                    <div class="col-md-5">
                                        <span class="d-inline-block w-25">₫72.000</span>
                                        <span class="d-inline-block w-25">1</span>
                                        <span class="float-right">₫72.000</span>
                                    </div>
                                </div>
                                <div class="item d-flex">
                                    <div class="col-md-7">
                                        <img style="width: 40px; height: 40px" src="https://cf.shopee.vn/file/fc9c996c33e5e0a6c8920fb00526db0d_tn">
                                        <span class="name d-inline-block">Quần Âu Nam Ikemen Côn Ống Dáng Ôm Vả dndd dmdmd mdmdmd</span>
                                        <span class="type d-inline-block">Loại: - GHI SÁNG -,SIZE 29</span>
                                    </div>
                                    <div class="col-md-5">
                                        <span class="d-inline-block w-25">₫72.000</span>
                                        <span class="d-inline-block w-25">1</span>
                                        <span class="float-right">₫72.000</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="voucher w-100 d-flex">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <i aria-hidden="true" class="fa fa-bookmark-o" style="color: rgb(238, 77, 45); font-size: 20px;"></i>
                            <span>Voucher của Shop</span>
                            <span style="float: right; color: #05a">Chọn Voucher</span>
                        </div>
                    </div>
                    <div class="transport-info w-100 d-flex">
                        <div class="col-md-4 note">
                            <span>Lời nhắn:</span>
                            <input type="text" placeholder="Lưu ý cho Người bán...">
                        </div>
                        <div class="col-md-8 info">
                            <span class="d-inline-block" style="width: 20%; color: #00bfa5">Đơn vị vận chuyển:</span>
                            <div style="width: 50%; padding-left: 20px">
                                <p style="margin-bottom: .3125rem; font-size: 1rem; font-weight: 600">Vận Chuyển Nhanh</p>
                                <p style="font-size: 0.75rem; font-weight: 400; margin: 0">Giao Hàng Tiết Kiệm</p>
                                <p style="font-weight: 400; margin-top: .3125rem;color: #ff424f">Vui lòng chọn thời gian giao hàng mong muốn</p>
                            </div>
                            <span class="d-inline-block text-center" style="width: 15%; color: #05a; text-transform: uppercase; cursor: pointer">Thay đổi</span>
                            <span class="d-inline-block text-center" style="width: 15%">₫37.700</span>
                        </div>
                    </div>
                    <div class="total-price w-100">
                        <div class="">
                            <span style="font-size: .875rem; color: #929292">Tổng số tiền (1 sản phẩm):</span>
                            <span style="font-size: 1.25rem; margin-left: 1.25rem; color: #ee4d2d">₫123.000</span>
                        </div>
                    </div>
                </div>
                <div class="payment-method w-100">
                    <div class="method">
                        <span>Phương thức thanh toán</span>
                        <button>Thanh toán khi nhận hàng</button>
                    </div>
                    <div class="txt-method">
                        <span style="width: 12.5rem; display: inline-block">Thanh toán khi nhận hàng</span>
                        <span>Cash on Delivery</span>
                    </div>
                    <div class="txt-sum-price w-100">
                       <div class="col-md-9"></div>
                        <div class="col-md-3">
                            <div class="total">
                                <span class="txt-pr-left">Tổng tiền hàng</span>
                                <span class="number-price">₫253.000</span>
                            </div>
                            <div class="price-fee">
                                <span class="txt-pr-left">Phí vận chuyển</span>
                                <span class="number-price">₫54.700</span>
                            </div>
                            <div class="payment">
                                <span class="txt-pr-left" style="vertical-align: super;width: 50%">Tổng thanh toán:</span>
                                <span class="number-price" style="height: 3.125rem;font-size: 1.75rem;color: #ee4d2d;">₫307.700</span>
                            </div>
                        </div>
                    </div>
                    <div class="order w-100">
                        <div class="col-md-9"></div>
                        <div class="col-md-3">
                            <button>Đặt hàng</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

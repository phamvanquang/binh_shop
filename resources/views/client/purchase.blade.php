@extends('layouts.app')

@section('content')
    <div class="tp-purchase">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div class="navbar-left">
                        <div class="profile d-flex">
                            <img src="https://cf.shopee.vn/file/c61c0184a814c00869b1be8338d6cd28_tn">
                            <div class="edit">
                                <p class="name">quangpv98</p>
                                <p class="txt-edit"><i class="fa fa-pencil" aria-hidden="true"></i>Sửa hồ sơ</p>
                            </div>
                        </div>
                        <div class="menu-left">
                            <ul>
                                <li>
                                    <img src="{{asset('images/account.png')}}">
                                    <a>Tài khoản của tôi</a>
                                </li>
                                <li>
                                    <img src="{{asset('images/donmua.png')}}">
                                    <a>Đơn Mua</a>
                                </li>
                                <li>
                                    <img src="{{asset('images/icon-noti.png')}}">
                                    <a>Thông báo</a>
                                </li>
                                <li>
                                    <img src="{{asset('images/voucher.png')}}">
                                    <a>Ví Voucher</a>
                                </li>
                                <li>
                                    <img src="{{asset('images/xu.png')}}">
                                    <a>Shopee Xu</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="navbar-menu">
                        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding: 0">
                            <div class="collapse navbar-collapse" id="navbarNavAltMarkup" style="background: #fff">
                                <div class="navbar-nav w-100">
                                    <a class="nav-item nav-link active" href="#">Tất cả <span class="sr-only">(current)</span></a>
                                    <a class="nav-item nav-link" href="#">Chờ xác nhận</a>
                                    <a class="nav-item nav-link" href="#">Chờ lấy hàng</a>
                                    <a class="nav-item nav-link" href="#">Đang giao</a>
                                    <a class="nav-item nav-link" href="#">Đã giao</a>
                                    <a class="nav-item nav-link" href="#">Đã Hủy</a>
                                </div>
                            </div>
                        </nav>
                    </div>
                    <div class="purchase-search">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <input class="ip-purchase-search" type="text" placeholder="Tìm kiếm theo Tên Shop, ID đơn hàng hoặc Tên Sản phẩm">
                    </div>
                    <div class="store-list-item">
                        <div class="store">
                            <div class="store-info">
                                <img style="width: 24px; height: 24px" src="https://cf.shopee.vn/file/a2385436c0c880974adbf48b1b1c1c38_tn">
                                <span class="store-name">ulzzang.official</span>
                                <button class="btn-chat"><i style="margin-right: 3px" class="fa fa-comment" aria-hidden="true"></i>chat</button>
                                <button class="btn-see-shop"><i style="margin-right: 3px" class="fa fa-shopping-basket" aria-hidden="true"></i>xem shop</button>
                                <span class="status float-right">Đã giao</span>
                            </div>
                            <div class="list-item">
                                <div class="item d-flex">
                                    <img style="width: 80px; height: 80px; margin-right: .75rem" src="https://cf.shopee.vn/file/4c9c05be289167e68918129445b2650b_tn">
                                    <div style="width: 90%" class="item-info">
                                        <p class="name">Áo thun dài tay NELLY cotton 100% dáng unisex form rộng in hình faxkti</p>
                                        <p class="type">Phân loại hàng: Đen,L</p>
                                        <p class="quantity">x 1</p>
                                    </div>
                                    <span class="price">₫118.000</span>
                                </div>
                                <div class="item d-flex">
                                    <img style="width: 80px; height: 80px; margin-right: .75rem" src="https://cf.shopee.vn/file/4c9c05be289167e68918129445b2650b_tn">
                                    <div style="width: 90%" class="item-info">
                                        <p class="name">Áo thun dài tay NELLY cotton 100% dáng unisex form rộng in hình faxkti</p>
                                        <p class="type">Phân loại hàng: Đen,L</p>
                                        <p class="quantity">x 1</p>
                                    </div>
                                    <span class="price">₫118.000</span>
                                </div>
                            </div>
                            <div class="store-footer">
                                <div class="total-price">
                                    <img style="width: 22px; height: 22px; margin-right: .3125rem" src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg/assets/bca2e6323e918c445dfe0279ebbc2c39.png">
                                    <span class="txt-price">Tổng số tiền:</span>
                                    <span class="price">₫153.100</span>
                                </div>
                                <div class="wrap-button">
                                    <button class="btn-rate">Đánh giá</button>
                                    <button class="btn-detail">Xem Chi tiết đơn hàng</button>
                                    <button class="btn-buy-again">Mua lần nữa</button>
                                </div>
                            </div>
                        </div>
                        <div class="store">
                            <div class="store-info">
                                <img style="width: 24px; height: 24px" src="https://cf.shopee.vn/file/a2385436c0c880974adbf48b1b1c1c38_tn">
                                <span class="store-name">ulzzang.official</span>
                                <button class="btn-chat"><i style="margin-right: 3px" class="fa fa-comment" aria-hidden="true"></i>chat</button>
                                <button class="btn-see-shop"><i style="margin-right: 3px" class="fa fa-shopping-basket" aria-hidden="true"></i>xem shop</button>
                                <span class="status float-right">Đã giao</span>
                            </div>
                            <div class="list-item">
                                <div class="item d-flex">
                                    <img style="width: 80px; height: 80px; margin-right: .75rem" src="https://cf.shopee.vn/file/4c9c05be289167e68918129445b2650b_tn">
                                    <div style="width: 90%" class="item-info">
                                        <p class="name">Áo thun dài tay NELLY cotton 100% dáng unisex form rộng in hình faxkti</p>
                                        <p class="type">Phân loại hàng: Đen,L</p>
                                        <p class="quantity">x 1</p>
                                    </div>
                                    <span class="price">₫118.000</span>
                                </div>
                                <div class="item d-flex">
                                    <img style="width: 80px; height: 80px; margin-right: .75rem" src="https://cf.shopee.vn/file/4c9c05be289167e68918129445b2650b_tn">
                                    <div style="width: 90%" class="item-info">
                                        <p class="name">Áo thun dài tay NELLY cotton 100% dáng unisex form rộng in hình faxkti</p>
                                        <p class="type">Phân loại hàng: Đen,L</p>
                                        <p class="quantity">x 1</p>
                                    </div>
                                    <span class="price">₫118.000</span>
                                </div>
                            </div>
                            <div class="store-footer">
                                <div class="total-price">
                                    <img style="width: 22px; height: 22px; margin-right: .3125rem" src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg/assets/bca2e6323e918c445dfe0279ebbc2c39.png">
                                    <span class="txt-price">Tổng số tiền:</span>
                                    <span class="price">₫153.100</span>
                                </div>
                                <div class="wrap-button">
                                    <button class="btn-rate" data-toggle="modal" data-target="#modalReviewProduct">Đánh giá</button>
                                    <button class="btn-detail">Xem Chi tiết đơn hàng</button>
                                    <button class="btn-buy-again">Mua lần nữa</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalReviewProduct" tabindex="-1" role="dialog" aria-labelledby="modalReviewProduct" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                    <p>hahahahahaha</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="tp-product-detail">
        <div class="container">
            <div class="row">
                <div class="tp-title">
                    Shopee > Thời Trang Nam > Quần > Quần Jean > QJ035 - QUẦN JEAN BAGGY NAM BASIC XANH BẠC
                </div>
            </div>
        </div>
        <div class="container" style="background: #fff; margin-top: 1.25rem;">
            <div class="row">
                <div class="col-md-5">
                    <div class="wrap-img-product">
                        <img src="{{asset('images/product-1.jpeg')}}">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="wrap-detail">
                        <div class="title-product">
                            <span class="txt-like">Yêu thích</span>
                            <span class="title">Áo Khoác Nam Trơn Có Mũ</span>
                        </div>
                        <div class="rate-product d-flex">
                            <div class="rate-start">
                                <span class="txt-star">4.7</span>
                                <span class="star">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </span>
                            </div>
                            <div class="border-line-rate"></div>
                            <div class="txt-rate">
                                <span>113</span>
                                <span>đánh giá</span>
                            </div>
                            <div class="border-line-rate"></div>
                            <div class="txt-sold">
                                <span>307</span>
                                <span>đã bán</span>
                            </div>
                        </div>
                        <div class="price-product">
                            <div class="price d-flex">
                                <div class="price-cost">₫199.000</div>
                                <div class="price-discount">₫139.000</div>
                                <div class="discount"><span>30% giảm</span></div>
                            </div>
                            <div class="txt-bottom">
                                <img src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg/assets/d7cb3c110ef860cca3969ab0cd6c2ac9.png">
                                <span>Ở đâu rẻ hơn, Shopee hoàn tiền</span>
                            </div>
                        </div>
                        <div class="detail">
                            <div class="row code-discount">
                                <div class="col-md-3">
                                    <span>Mã Giảm Giá Của Shop</span>
                                </div>
                                <div class="col-md-9">
                                    <span>10% giảm</span>
                                    <span>15% giảm</span>
                                    <span>20% giảm</span>
                                </div>
                            </div>
                            <div class="row transport">
                                <div class="col-md-3">
                                    <span>Vận Chuyển</span>
                                </div>
                                <div class="col-md-9">
                                    <div>
                                        <span>
                                            <img style="width: 25px; height: 15px" src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg/assets/9d21899f3344277e34d40bfc08f60bc7.png">
                                            Miễn Phí Vận Chuyển
                                        </span>
                                        <p>Miễn Phí Vận Chuyển khi đơn đạt giá trị tối thiểu</p>
                                    </div>
                                    <div>
                                        <span style="color: #636363; margin-right: 15px">
                                            <img style="width: 25px; height: 15px" src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg/assets/9d21899f3344277e34d40bfc08f60bc7.png">
                                            Vận Chuyển Tới
                                        </span>
                                        <span style="color: #222; margin-right: 3px">Huyện Ba Vì, Hà Nội</span>
                                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        <p>
                                            <span style="color: #636363; margin-right: 15px">Phí Vận Chuyển</span>
                                            <span style="color: #222"; margin-right: 3px>₫8.500 - ₫35.500</span>
                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row color">
                                <div class="col-md-3">
                                    <span>Màu sắc</span>
                                </div>
                                <div class="col-md-9">
                                    <button class="variation">Xanh Bạc</button>
                                    <button class="variation">Xanh Bạc</button>
                                    <button class="variation">Xanh Bạc</button>
                                </div>
                            </div>
                            <div class="row size">
                                <div class="col-md-3">
                                    <span>Kích cỡ</span>
                                </div>
                                <div class="col-md-9">
                                    <button class="variation">S</button>
                                    <button class="variation">M</button>
                                    <button class="variation">XL</button>
                                </div>
                            </div>
                            <div class="row count">
                                <div class="col-md-3">
                                    <span>Số lượng</span>
                                </div>
                                <div class="col-md-9 d-flex">
                                    <button>
                                        <svg enable-background="new 0 0 10 10" viewBox="0 0 10 10" x="0" y="0" class="shopee-svg-icon "><polygon points="4.5 4.5 3.5 4.5 0 4.5 0 5.5 3.5 5.5 4.5 5.5 10 5.5 10 4.5"></polygon></svg>
                                    </button>
                                    <input type="text" value="1">
                                    <button>
                                        <svg enable-background="new 0 0 10 10" viewBox="0 0 10 10" x="0" y="0" class="shopee-svg-icon icon-plus-sign"><polygon points="10 4.5 5.5 4.5 5.5 0 4.5 0 4.5 4.5 0 4.5 0 5.5 4.5 5.5 4.5 10 5.5 10 5.5 5.5 10 5.5"></polygon></svg>
                                    </button>
                                    <div style="padding-top: 10px; margin-left: 15px"><span>317 sản phẩm có sẵn</span></div>
                                </div>
                            </div>
                            <div class="row wrap-button">
                                <button class="btn-add-cart">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                    thêm vào giỏ hàng
                                </button>
                                <button class="btn-buy-now">Mua ngay</button>
                            </div>
                            <div class="row wrap-footer">
                                <img src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg/assets/bca2e6323e918c445dfe0279ebbc2c39.png">
                                <span class="guaranteed">Shopee Đảm Bảo</span>
                                <span>3 Ngày Trả Hàng / Hoàn Tiền</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="tp-cart">
        <div class="container">
            <div class="row">
                <div class="note-discount">
                    <img style="width: 23px" src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg/assets/9d21899f3344277e34d40bfc08f60bc7.png">
                    <span>Nhấn vào mục Mã giảm giá ở cuối trang để hưởng miễn phí vận chuyển bạn nhé!</span>
                </div>
                <div class="wrap-title">
                    <div class="col-md-6" style="padding-left: 45px">
                        <input class="form-check-input all-product" type="checkbox" value="">
                        <span class="txt-product">Sản Phẩm</span>
                    </div>
                    <div class="col-md-6 d-flex">
                        <div class="unit-price" style="width: 30%">
                            Đơn Giá
                        </div>
                        <div class="quantity" style="width: 30%">
                            Số Lượng
                        </div>
                        <div class="price" style="width: 20%">
                            Số Tiền
                        </div>
                        <div class="action" style="width: 20%">
                            Thao Tác
                        </div>
                    </div>
                </div>
                <div class="group-item-store w-100">
                    <div class="store w-100">
                        <div class="header w-100">
                            <div class="col-md-6" style="padding-left: 45px">
                                <input class="form-check-input all-product-store" type="checkbox" value="">
                                <span class="txt-like">Yeu thich</span>
                                <span class="name-store">Quần Tây Nam TCE</span>
                            </div>
                        </div>
                        <div class="list-item">
                            <div class="item">
                                <div class="col-md-6 d-flex" style="padding-left: 45px; align-items: center">
                                    <div class="wrap-img">
                                        <input class="form-check-input check-item" type="checkbox" value="">
                                        <img style="width: 80px; height: 80px" src="https://cf.shopee.vn/file/c2670ab82b57bb0b305f73f37474907c_tn">
                                    </div>
                                    <span>Tai nghe Bluetooth không dây Inpods i12 TWS điều khiển cảm biến âm thanh HIFI cho Android iOS</span>
                                </div>
                                <div class="col-md-6 d-flex">
                                    <span class="price" style="width: 30%">₫90.000</span>
                                    <div class="quantity" style="width: 30%">
                                        <button>
                                            <svg enable-background="new 0 0 10 10" viewBox="0 0 10 10" x="0" y="0" class="shopee-svg-icon "><polygon points="4.5 4.5 3.5 4.5 0 4.5 0 5.5 3.5 5.5 4.5 5.5 10 5.5 10 4.5"></polygon></svg>
                                        </button>
                                        <input type="text" value="1">
                                        <button>
                                            <svg enable-background="new 0 0 10 10" viewBox="0 0 10 10" x="0" y="0" class="shopee-svg-icon icon-plus-sign"><polygon points="10 4.5 5.5 4.5 5.5 0 4.5 0 4.5 4.5 0 4.5 0 5.5 4.5 5.5 4.5 10 5.5 10 5.5 5.5 10 5.5"></polygon></svg>
                                        </button>
                                    </div>
                                    <span class="total-price" style="width: 20%">₫90.000</span>
                                    <span class="delete-item" style="width: 20%">
                                        Xóa
                                    </span>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-md-6 d-flex" style="padding-left: 45px; align-items: center">
                                    <div class="wrap-img">
                                        <input class="form-check-input check-item" type="checkbox" value="">
                                        <img style="width: 80px; height: 80px" src="https://cf.shopee.vn/file/c2670ab82b57bb0b305f73f37474907c_tn">
                                    </div>
                                    <span>Tai nghe Bluetooth không dây Inpods i12 TWS điều khiển cảm biến âm thanh HIFI cho Android iOS</span>
                                </div>
                                <div class="col-md-6 d-flex">
                                    <span class="price" style="width: 30%">₫90.000</span>
                                    <div class="quantity" style="width: 30%">
                                        <button>
                                            <svg enable-background="new 0 0 10 10" viewBox="0 0 10 10" x="0" y="0" class="shopee-svg-icon "><polygon points="4.5 4.5 3.5 4.5 0 4.5 0 5.5 3.5 5.5 4.5 5.5 10 5.5 10 4.5"></polygon></svg>
                                        </button>
                                        <input type="text" value="1">
                                        <button>
                                            <svg enable-background="new 0 0 10 10" viewBox="0 0 10 10" x="0" y="0" class="shopee-svg-icon icon-plus-sign"><polygon points="10 4.5 5.5 4.5 5.5 0 4.5 0 4.5 4.5 0 4.5 0 5.5 4.5 5.5 4.5 10 5.5 10 5.5 5.5 10 5.5"></polygon></svg>
                                        </button>
                                    </div>
                                    <span class="total-price" style="width: 20%">₫90.000</span>
                                    <span class="delete-item" style="width: 20%">
                                        Xóa
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="store w-100">
                        <div class="header w-100">
                            <div class="col-md-6" style="padding-left: 45px">
                                <input class="form-check-input all-product-store" type="checkbox" value="">
                                <span class="txt-like">Yeu thich</span>
                                <span class="name-store">Quần Tây Nam TCE</span>
                            </div>
                        </div>
                        <div class="list-item">
                            <div class="item">
                                <div class="col-md-6 d-flex" style="padding-left: 45px; align-items: center">
                                    <div class="wrap-img">
                                        <input class="form-check-input check-item" type="checkbox" value="">
                                        <img style="width: 80px; height: 80px" src="https://cf.shopee.vn/file/c2670ab82b57bb0b305f73f37474907c_tn">
                                    </div>
                                    <span>Tai nghe Bluetooth không dây Inpods i12 TWS điều khiển cảm biến âm thanh HIFI cho Android iOS</span>
                                </div>
                                <div class="col-md-6 d-flex">
                                    <span class="price" style="width: 30%">₫90.000</span>
                                    <div class="quantity" style="width: 30%">
                                        <button>
                                            <svg enable-background="new 0 0 10 10" viewBox="0 0 10 10" x="0" y="0" class="shopee-svg-icon "><polygon points="4.5 4.5 3.5 4.5 0 4.5 0 5.5 3.5 5.5 4.5 5.5 10 5.5 10 4.5"></polygon></svg>
                                        </button>
                                        <input type="text" value="1">
                                        <button>
                                            <svg enable-background="new 0 0 10 10" viewBox="0 0 10 10" x="0" y="0" class="shopee-svg-icon icon-plus-sign"><polygon points="10 4.5 5.5 4.5 5.5 0 4.5 0 4.5 4.5 0 4.5 0 5.5 4.5 5.5 4.5 10 5.5 10 5.5 5.5 10 5.5"></polygon></svg>
                                        </button>
                                    </div>
                                    <span class="total-price" style="width: 20%">₫90.000</span>
                                    <span class="delete-item" style="width: 20%">
                                        Xóa
                                    </span>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-md-6 d-flex" style="padding-left: 45px; align-items: center">
                                    <div class="wrap-img">
                                        <input class="form-check-input check-item" type="checkbox" value="">
                                        <img style="width: 80px; height: 80px" src="https://cf.shopee.vn/file/c2670ab82b57bb0b305f73f37474907c_tn">
                                    </div>
                                    <span>Tai nghe Bluetooth không dây Inpods i12 TWS điều khiển cảm biến âm thanh HIFI cho Android iOS</span>
                                </div>
                                <div class="col-md-6 d-flex">
                                    <span class="price" style="width: 30%">₫90.000</span>
                                    <div class="quantity" style="width: 30%">
                                        <button>
                                            <svg enable-background="new 0 0 10 10" viewBox="0 0 10 10" x="0" y="0" class="shopee-svg-icon "><polygon points="4.5 4.5 3.5 4.5 0 4.5 0 5.5 3.5 5.5 4.5 5.5 10 5.5 10 4.5"></polygon></svg>
                                        </button>
                                        <input type="text" value="1">
                                        <button>
                                            <svg enable-background="new 0 0 10 10" viewBox="0 0 10 10" x="0" y="0" class="shopee-svg-icon icon-plus-sign"><polygon points="10 4.5 5.5 4.5 5.5 0 4.5 0 4.5 4.5 0 4.5 0 5.5 4.5 5.5 4.5 10 5.5 10 5.5 5.5 10 5.5"></polygon></svg>
                                        </button>
                                    </div>
                                    <span class="total-price" style="width: 20%">₫90.000</span>
                                    <span class="delete-item" style="width: 20%">
                                        Xóa
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swap-footer w-100">
                    <div class="voucher d-flex">
                        <div class="col-md-6"></div>
                        <div class="col-md-6 d-flex">
                            <div class="w-50 text-center">
                                <i style="color: #ee4d2d; font-size: 20px" class="fa fa-bookmark-o" aria-hidden="true"></i>
                                <span style="font-size: 1rem; font-weight: 500; vertical-align: bottom; margin-left: 5px">Shopee Voucher</span>
                            </div>
                            <div class="w-50 text-center">
                                <span>Chọn Hoặc Nhập Mã</span>
                            </div>
                        </div>
                    </div>
                    <div class="summary d-flex">
                        <div class="col-md-5">
                            <input style="width: 16px; height: 16px" class="form-check-input" type="checkbox" value="">
                            <span class="check-all">Chọn tất cả (3)</span>
                            <span class="delete">Xóa</span>
                        </div>
                        <div class="col-md-7">
                            <span>Tổng tiền hàng (3 sản phẩm):</span>
                            <span class="total-price">₫237.000</span>
                            <button class="btn-buy">Mua hàng</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

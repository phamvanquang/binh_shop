<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('font-awesome-4.7.0/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
{{--        <div class="header-login">--}}
{{--            <div class="container">--}}
{{--                <div class="row">--}}
{{--                    <div class="header-left w-50">--}}
{{--                        <img src="{{asset('images/logo-login.png')}}">--}}
{{--                        <span>Đăng nhập</span>--}}
{{--                    </div>--}}
{{--                    <div class="header-right w-50 text-right">--}}
{{--                        <span>Cần trợ giúp?</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="header-home-page">
            <div class="container">
                <div class="row">
                    <div class="header-top w-100 d-flex">
                        <div class="col-md-4">
                            <span class="txt-add-border">Kênh Người Bán</span>
                            <span class="txt-add-border">Tải ứng dụng</span>
                            <span>Kết nối</span>
                            <img src="{{asset('images/fb-icon-home.png')}}">
                            <img src="{{asset('images/insta-icon-home.png')}}">
                        </div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4 text-right">
                            <img src="{{asset('images/bell.png')}}">
                            <span style="padding-left: 0">Thông báo</span>
                            <img style="margin-left: 5px" src="{{asset('images/help.png')}}">
                            <span style="padding-left: 0">Trợ giúp</span>
                            <span class="txt-add-border">Đăng ký</span>
                            <span><a href="{{route('login')}}">Đăng nhập</a></span>
                        </div>
                    </div>
                    <div class="wrap-search w-100 d-flex">
                        <div class="col-md-2">
                            <img src="{{asset('images/logo-home.png')}}">
                        </div>
                        <div class="col-md-8">
                            <input class="ip-search" type="text">
                            <button class="btn-search"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                        <div style="width: 10%;text-align: center;margin-top: 14px;" class="col-m-2">
                            <img src="{{asset('images/cart.png')}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
{{--        <div class="header-other-page">--}}
{{--            <div class="container">--}}
{{--                <div class="row">--}}
{{--                    <div class="header-top w-100 d-flex">--}}
{{--                        <div class="col-md-4">--}}
{{--                            <span class="txt-add-border">Kênh Người Bán</span>--}}
{{--                            <span class="txt-add-border">Tải ứng dụng</span>--}}
{{--                            <span>Kết nối</span>--}}
{{--                            <img src="{{asset('images/fb-icon-home.png')}}">--}}
{{--                            <img src="{{asset('images/insta-icon-home.png')}}">--}}
{{--                        </div>--}}
{{--                        <div class="col-md-4"></div>--}}
{{--                        <div class="col-md-4 text-right">--}}
{{--                            <img src="{{asset('images/bell.png')}}">--}}
{{--                            <span style="padding-left: 0">Thông báo</span>--}}
{{--                            <img style="margin-left: 5px" src="{{asset('images/help.png')}}">--}}
{{--                            <span style="padding-left: 0">Trợ giúp</span>--}}
{{--                            <span class="txt-add-border">Đăng ký</span>--}}
{{--                            <span><a href="{{route('login')}}">Đăng nhập</a></span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="wrap-search w-100 d-flex" style="background: #fff">--}}
{{--                <div class="container">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-md-6">--}}
{{--                            <img style="margin-left: 1.125rem" src="{{asset('images/logo-login.png')}}">--}}
{{--                            <span class="title-page">Giỏ Hàng</span>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-6">--}}
{{--                            <input type="text" class="ip-search">--}}
{{--                            <button class="btn-search"><i class="fa fa-search" aria-hidden="true"></i></button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

        <main class="">
            @yield('content')
        </main>
    </div>
</body>
</html>

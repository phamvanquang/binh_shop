@extends('layouts.app')

@section('content')
<div class="tp-login">
    <div class="container">
        <div class="row">
            <div class="col-md-6">

            </div>
            <div class="col-md-6" style="max-width: 530px">
                <div class="form-login">
                    <p class="title-login">Đăng nhập</p>
                    <div class="form-group ip-email">
                        <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Email/Số điện thoại/Tên đăng nhập">
                    </div>
                    <div class="form-group ip-password">
                        <input type="password" class="form-control" placeholder="Mật khẩu">
                    </div>
                    <button type="submit" class="btn btn-primary btn-login">Đăng nhập</button>
                    <div class="forgot-password d-flex">
                        <div class="w-50">
                            <p>Quên mật khẩu</p>
                        </div>
                        <div class="w-50 text-right">
                            <p>Đăng nhập với SMS</p>
                        </div>
                    </div>
                    <div class="border-line d-flex">
                        <div></div>
                        <span>HOẶC</span>
                        <div></div>
                    </div>
                    <div class="social-login d-none d-md-flex">
                        <div class="img-face">
                            <img src="{{asset('images/facebook.png')}}">
                        </div>
                        <div class="img-google">
                            <img src="{{asset('images/google.png')}}">
                        </div>
                        <div class="img-apple">
                            <img src="{{asset('images/apple.png')}}">
                        </div>
                    </div>
                    <div class="swap-footer">
                        <span>Bạn mới biết đến Shopee?</span>
                        <span class="txt-register">Đăng ký</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
